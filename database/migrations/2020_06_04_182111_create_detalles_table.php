<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sf_doc_electronico_det', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_doc_electronico');
            $table->integer('ordenItem');
            $table->integer('codigoProductoItem');
            $table->integer('descripcionItem');
            $table->integer('unidadMedidaItem');
            $table->integer('cantidadItem');
            $table->integer('valorUnitarioSinIgv');
            $table->integer('precioUnitarioConIgv');
            $table->integer('codTipoPrecioVtaUnitarioItem');
            $table->integer('importeIGVItem');
            $table->integer('codigoAfectacionIGVItem');
            $table->integer('descuentoItem');
            $table->integer('valorVentaItem');
            $table->integer('_descuentoItem');
            $table->integer('_precioUnitario');
            $table->integer('_valorVentaSinIGV');
            $table->integer('montoReferenciaItem');
            $table->integer('montoReferencialUnitarioItem');
            $table->integer('clasificacionProductoItem');
            $table->integer('montoTotalItem');
            $table->integer('cantidadBolsasItem');
            $table->integer('montoUnitarioBolsasItem');
            $table->integer('importeBolsasItem');
            $table->integer('iddetpedido');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sf_doc_electronico_det');
    }
}

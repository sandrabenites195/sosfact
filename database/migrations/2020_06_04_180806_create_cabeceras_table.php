<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCabecerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sf_doc_electronico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_doc_electronico');
            $table->string('serieNumero',13);
            $table->integer('tipoDocumento');     
            $table->date('fechaEmision');
            $table->char('numeroDocIdentidadEmisor',100);
            $table->integer('tipoDocIdentidadEmisor');
            $table->string('numeroDocIdentidadReceptor',100);    
            $table->string('razonSocialReceptor',450);    
            $table->string('direccionReceptor',450);      
            $table->string('correoReceptor',450);    
            $table->integer('tipoDocIdentidadReceptor');     
            $table->char('telefono',11);   
            $table->integer('idCliente');     
            $table->double('totalOPGravadas',26,2);   
            $table->double('totalOPExoneradas',26,2); 
            $table->double('totalOPNoGravadas',26,2);
            $table->double('totalOPGratuitas',26,2);
            $table->double('sumatoriaIGV',26,2);
            $table->double('sumatoriaISC',26,2);
            $table->double('importeTotal',26,2);
            $table->double('importeTotalVenta',26,2);
            $table->double('totalDescuentos',26,2);
            $table->double('descuentosGlobales',26,2);
            $table->double('sumatoriaOtrosCargos',26,2);
            $table->double('porcentajeOtrosCargos',26,2);
            $table->double('sumatoriaImpuestoBolsas',26,2);
            $table->char('tipoMoneda',100);
            $table->char('codigoPaisReceptor',100);
            $table->integer('codigoTipoOperacion');
            $table->char('montoEnLetras',150);
            $table->integer('idPedido'); 
            $table->integer('Doc_Estado'); 
            $table->integer('Doc_id_cierre');   
                    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sf_doc_electronico');
    }
}

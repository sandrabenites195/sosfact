<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Cabecera;
use App\Detalle;

class formularioController extends Controller
{
    //
    public function index(){

        return view('formulario', ["error" => FALSE]);
    }

    public function store(Request $request ){
   
        $request->validate([
            'tipoDocumento'   => 'required',
            'serieNumero'     => 'required',
            'rucEmisor'       => 'required',
            'importeTotal'    => 'required',
            'fechaEmision'    => 'required' 
        ]);
        
        $tipoDocumento       =  $request->get('tipoDocumento');
        $serieNumero         =  $request->get('serieNumero');
        $rucEmisor           =  $request->get('rucEmisor');  
        $importeTotal        =  $request->get('importeTotal'); 
        $fechaEmision        =  $request->get('fechaEmision');

    

        $cabeceras = Cabecera::where([
        ['tipoDocumento', '=', $request->tipoDocumento],
        ['serieNumero', '=',   $request->serieNumero],
        ['numeroDocIdentidadEmisor', '=',$request->rucEmisor],
        ['importeTotal', '=',$request->importeTotal],
        ['fechaEmision', '=',$request->fechaEmision],
        ])
        ->with('detalles')
        ->get();


        if (count($cabeceras)>0) {
            return view('respuesta',compact('cabeceras'));
        } else {
            return view('formulario', ["error" => TRUE]);
        }
        
    }

   
}

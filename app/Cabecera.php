<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabecera extends Model
{
    //
    protected $table ="sf_doc_electronico";
   // protected $primaryKey = "id_doc_electronico";

    protected $fillable =[
            "id_doc_electronico",
            "serieNumero",
            "tipoDocumento",
            "fechaEmision", 
            "numeroDocIdentidadEmisor",
            "tipoDocIdentidadEmisor",
            "numeroDocIdentidadReceptor",
            "razonSocialReceptor",
            "direccionReceptor",
            "correoReceptor",
            "tipoDocIdentidadReceptor",
            "telefono",
            "idCliente",  
            "totalOPGravadas",  
            "totalOPExoneradas ", 
            "totalOPNoGravadas ", 
            "totalOPGratuitas ",
            "sumatoriaIGV ", 
            "sumatoriaISC ", 
            "importeTotal ", 
            "importeTotalVenta",
            "totalDescuentos", 
            "descuentosGlobales" , 
            "sumatoriaOtrosCargos",  
            "porcentajeOtrosCargos",  
            "sumatoriaImpuestoBolsas",  
            "tipoMoneda",
            "codigoPaisReceptor",
            "codigoTipoOperacion",
            "montoEnLetras",
            "idPedido",
            "Doc_Estado",
            "Doc_id_cierre",  
    ];
    //relacion de 1 - m con la tabla cabecera
    public function detalles(){
        return $this->hasMany('App\Detalle' ,'id_doc_electronico' );
    }


     
}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model
{
    //
    protected $table = "sf_doc_electronico_det";

    protected $fillable = [

        "id_doc_electronico",
        "ordenItem",
        "codigoProductoItem",
        "descripcionItem",
        "unidadMedidaItem",
        "cantidadItem",
        "valorUnitarioSinIgv",
        "precioUnitarioConIgv",
        "codTipoPrecioVtaUnitarioItem",
        "importeIGVItem",
        "codigoAfectacionIGVItem",
        "descuentoItem",
        "valorVentaItem",
        "_descuentoItem",
        "_precioUnitario",
        "_valorVentaSinIGV",
        "montoReferenciaItem",
        "montoReferencialUnitarioItem",
        "clasificacionProductoItem",
        "montoTotalItem",
        "cantidadBolsasItem",
        "montoUnitarioBolsasItem",
        "importeBolsasItem",
        "iddetpedido",


    ];
        
    
}

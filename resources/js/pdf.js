var pdfMake = require('pdfmake/build/pdfmake.js');
var pdfFonts = require('pdfmake/build/vfs_fonts.js');
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import QRious from "qrious/dist/qrious"

var  docDefinition = null;

import { logo } from './logo'

var bodyDetalles = [];
var bodyPrecios = [];

bodyDetalles.push([ 
    { text: 'CODIGO', style: 'subtitle' }, 
    { text: 'DESCRIPCION', style: 'subtitle' },
    { text: 'CANT.', style: 'subtitle' },
    { text: 'UM', style: 'subtitle' },
    { text: 'V. UNITARIO', style: 'subtitle' },
    { text: 'P. UNITARIO', style: 'subtitle' },
    { text: 'DESCTO', style: 'subtitle' }, 
    { text: 'TOTAL', style: 'subtitle' },
]);

cabecera.detalles.forEach(detalle => {
    bodyDetalles.push([
        detalle.codigoProductoItem,
        detalle.descripcionItem,
        detalle.cantidadItem,
        detalle.unidadMedidaItem,
        detalle.valorUnitarioSinIgv,
        detalle.precioUnitarioConIgv,
        detalle._descuentoItem,
        detalle.montoTotalItem,
    ]);
});


bodyPrecios.push([
  { text: `son: ${cabecera.montoEnLetras} `, style: 'subtitle' },
  { text: 'OP. gravadas:', style: 'subtitle' },
  'S/',
  cabecera.totalOPGravadas,
]);

bodyPrecios.push([
  '',
  {text: 'OP. Inafectada:' ,style:'subtitle'},
  'S/',
  cabecera.totalOPNoGravadas,
]);
bodyPrecios.push([
  '',
  { text: 'OP. Exonerada:', style:'subtitle' },
  'S/',
  cabecera.totalOPExoneradas,
]);
bodyPrecios.push([
  '',
  {text: 'OP. Gratuita:' ,style:'subtitle'},
  'S/',
  cabecera.totalOPGratuitas,
]);
bodyPrecios.push([
  '',
  {text: 'IGV (18%):' ,style:'subtitle'},
  'S/',
  cabecera.sumatoriaIGV,
]);
bodyPrecios.push([
  '',
  {text: 'OTR. CAR(0.00%)' ,style:'subtitle'},
  'S/',
  cabecera.sumatoriaOtrosCargos,
]);

bodyPrecios.push([
  '',
  {text: 'IMPORTE TOTAL' ,style:'subtitle'},
  'S/',
  cabecera.importeTotal,
]);

bodyPrecios.push([
  '',
  {text: 'Percepcion' ,style:'subtitle'},
  'S/',
  '0.00'
]);

bodyPrecios.push([
  '',
  {text: 'Total a Cobrar' ,style:'subtitle'},
  'S/',
  cabecera.importeTotal,
]);

// var tipoComprobante = null;
// if (cabecera.tipoDocumento == 3) {
//   tipoComprobante = 'BOLETA DE VENTA  ELECTRONICA\n';
// } else {
//   tipoComprobante = 'FACTURA ELECTRONICA\n';
// }

docDefinition = {
  defaultStyle: {
    fontSize: 10,
    lineHeight: 0.8,
  },
  styles: {
    header: {
      fontSize: 18,
      bold: true
    },
    subheader: {
      fontSize: 12,
      bold: true
    },
    subtitle: {
      fontSize: 10,
      bold: true,
    }
  },
  pageSize: 'A4',
  pageOrientation: 'portrait',
  pageMargins: [ 20, 20, 20, 20 ],
  content: [
    {
        image: logo,
        width: 160
    },
    '\n',
    {
      text: cabecera.razonSocialReceptor,
      style: 'header',
    },
    '\n',
    {
      text: cabecera.direccionReceptor,
      style: 'subheader',
    },
    '\n',
    {
        absolutePosition: { x: 370, y: 20 },
        lineHeight: 1.2,
        table: {
          heights: [50, 50, 50],
          body: [[
            {
              text:[
                {
                  text: `RUC N°${cabecera.numeroDocIdentidadEmisor}\n`,
                  style: 'subheader',
                },
                {
                  text: cabecera.tipoDocumento == 1 ? 'FACTURA ELECTORNICA\n' : 'BOLETA DE VENTA ELECTRONICA\n',
                  style: 'subheader',
                },
                {
                  text: cabecera.serieNumero, 
                  alignment: 'center',
                  style: 'subtitle'
                },
              ], 
              alignment: 'center'
            },
          ]]
        },
    },
    {
        columns: [
            {   
                layout: 'noBorders',
                table: {
                    body: [
                        [{ text: 'SEÑOR (ES)', style: 'subheader' }, cabecera.razonSocialReceptor],
                        ['DOC.TRIB.NO.', cabecera.numeroDocIdentidadReceptor],
                        ['DIRECCION', cabecera.direccionReceptor],
                    ]
                }

            },
            {   
                layout: 'noBorders',
                table: {
                    body: [
                        ['FECHA EMISION', cabecera.fechaEmision],
                        ['MONEDA.', cabecera.tipoMoneda],
                        ['TELEFONO', cabecera.telefono],
                    ]
                }

            },
        ]
    },
    '\n',
    {
        layout: 'lightHorizontalLines',
        table: {
            widths: [ 50, '*', 50, 50, 50, 50, 50, 50 ],
            body: bodyDetalles,
        }
    },
    '\n',
    {
        layout: 'noBorders',
        table: {
            widths: [ '*', 100, 50, 50, ],
            body: bodyPrecios,
        }
    },
    {
        absolutePosition: { x: 20, y: 640 },
        image: getQR(),
        width: 160
    },
  ]
};

const pdfDocGenerator = pdfMake.createPdf(docDefinition);
pdfDocGenerator.getDataUrl((dataUrl) => {
  var iframe = document.querySelector('#viewPdf');
  iframe.src = dataUrl;
});

function getQR() {
  console.log(`${cabecera.numeroDocIdentidadEmisor}|
  ${cabecera.tipoDocumento == 1 ? 'FACTURA' : 'BOLETA'}|
  ${cabecera.serieNumero.slice(5, cabecera.serieNumero.length)}|
  ${cabecera.serieNumero.slice(0, 4)}|
  ${cabecera.importeTotal}|
  ${cabecera.fechaEmision}|`)
    var qr = new QRious({
      size: 500,
      value: `${cabecera.numeroDocIdentidadEmisor}|
              ${cabecera.tipoDocumento == 1 ? 'FACTURA' : 'BOLETA'}|
              ${cabecera.serieNumero.slice(5, cabecera.serieNumero.length)}|
              ${cabecera.serieNumero.slice(0, 4)}|
              ${cabecera.importeTotal}|
              ${cabecera.fechaEmision}|`
    });
    return qr.toDataURL()
  }

var downloadPdf = function() {
    pdfMake.createPdf(docDefinition).download(cabecera.serieNumero);
}

window.downloadPdf = downloadPdf;
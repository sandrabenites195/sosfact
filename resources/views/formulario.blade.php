<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sosfact</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>

        <header>
            <div>
                <div class="container-fluid">
                    <div class="row">
                      <div class="col-lg-6"><img src="{{url('/img/fact.jpeg')}}" width="40%" alt=""></div>
                        <div class="col-lg-6"><img src="{{url('/img/doc.jpeg')}}" style="width:40%; float:right" alt=""></div>
                    </div>
                
                </div>
            </div>
        </header>


 
<style>
     .header{
            width: 100%;
       
            background: #ffffffd9;
       
        }
        .container-fluid{
            width: 100%;
        }
        .row{
            display: flex;
        }
        .col-lg-6{
            float: left;
            width: 50%;
        }
        .row{
            padding: 20px 10px;
        }

    .newform .col-md-4.control-label{
        float: left;
    text-align: right;
    }
    .newform .col-md-6{
        float: left;
    }

    .newform .col-md-3.control-label{
        float: left;
    }
    .newform footer{
        border-top: 1px solid;
    padding: 5px 0;
    }
    .newform .panel-footer.text-center.mt-none{
        background: #f3f3f3;
    border-radius: 0 0 10px 10px;
        
    }
    .newform .header-logo{
        background: white;
    }
    .newform button{
        min-width: 150px;
    background: white;
    box-shadow: inset 1px -3px 1px grey;
    border: 1px solid grey;
    }
    .newform button#btn-limpiar i{
        opacity: 0;
        transform: translateX(30px);
    
        position: absolute;
    }
    .newform button#btn-limpiar:hover i{
        opacity: 1;
        transform: translateX(0px);
        transition: .5s;
        position: relative;
    }
    .newform #btn-submit{
        position: relative;
        overflow: hidden;
        transition: .3s;
        z-index: 2;
    }
    .newform #btn-submit:hover{
        webkit-box-shadow: 21px 21px 182px 28px rgba(43,170,177,1);
    -moz-box-shadow: 21px 21px 182px 28px rgba(43,170,177,1);
    box-shadow: 0px 2px 15px 2px #2baab1;
   
    border: 1px solid #70e6ec;
    color: white;
    transition: .8s;
    }


    .newform #btn-submit:hover::before{
        background: #2baab1;
    position: absolute;
    content: '';
    
    top: 0;
    width: 100%;
    z-index: -1;
    height: 100%;
    animation: botonconsult .5s linear forwards;
    }
@keyframes botonconsult{
    0%{
        left: -100%;
    }
    100%{
        left: 0;
    }
}

    .panel-title{
        background: #d0d0d0;
    padding: 10px 30px;
    border-bottom: 1px solid black;
    border-radius: 10px 10px 0 0px;
    }
    .panel.panel-admin{
        background: #fbfafa;
        border-radius: 10px;
    }
    .form-group{
        height: 30px;
    }
    .control-label{
        line-height: 38px;
    }
    .input-group-addon {
    padding: 0px 8px;
    line-height: 38px;
    background: gray;
    border-radius: 5px 0 0px 5px;
    color: white;
}
.datepicker{
    float: left;
}
input {
  
}
</style>




    <section>
        <div class="container">
           
    <section class="newform">
    <form id="demo-form" onsubmit="onSubmit(event)" class="form-horizontal form-bordered" method="GET" action="{{route('respuesta')}}">
            <section class="panel panel-admin">
                <header class="panel-heading">
                     <h2 class="panel-title">Ingrese sus datos:</h2>
                </header>
                <div class="panel-body pb-none">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tipo de Documento:</label>
                        <div class="col-md-6">
                            <div class="input-group btn-group"><span class="input-group-addon"><i class="fa fa-info-circle" data-plugin-tooltip="" data-toggle="tooltip"  data-placement="top" title=""
                              data-original-title="Escoja el tipo de documento que desea consultar"></i></span>
                                    <select id="tipoDocumento" name="tipoDocumento" class="form-control" required>
                                        <option value="00">ORDEN DE COMPRA</option>
                                        <option value="1">FACTURA</option>
                                        <option value="3">BOLETA</option>
                                        {{-- <option value="07">NOTA DE CRÉDITO</option>
                                        <option value="08">NOTA DE DÉBITO</option>
                                        <option value="40">PERCEPCIÓN</option>
                                        <option value="20">RETENCIÓN</option> --}}
                                </select>
                            </div>
                        </div>
                    </div>
                  <hr>
                    
                    <div id="grupo-sn" class="form-group"><label class="col-md-4 control-label">Serie -
                            Número:</label>
                        <div class="col-md-6 control-label">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-info-circle"
                                        data-plugin-tooltip="" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Ingrese la serie y el número del documento. No es necesario completar el número con ceros a la izquierda">
                                    </i></span>
                                    <input id="serieNumero" oninput="checkSerie(event.target)" name="serieNumero" class="form-control text-uppercase" maxlength="13" data-plugin-masked-input="" data-input-mask="a***-9?9999999" required></div>
                        </div>
                    
                    </div>
                    <div id="grupo-oc" class="form-group" hidden="true">
                        <label class="col-md-4 control-label">Orden de Compra:</label>
                        <div class="col-md-6 control-label">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-info-circle"
                                        data-plugin-tooltip="" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Ingrese el número de orden de compra asociado al documento.">
                                    </i> </span><input id="orden-compra" name="nu" class="form-control"></div>
                        </div>
                    </div>
                    <hr>
                    <div id="grupo-nde" class="form-group"><label class="col-md-4 control-label">RUC del
                            Emisor:</label>
                        <div class="col-md-6 control-label">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-info-circle"
                                        data-plugin-tooltip="" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Ingrese el RUC de la empresa que le emitió el documento"></i>
                                </span>
                                <input type="number" id="rucEmisor" oninput="checkRuc(event.target)" name="rucEmisor" class="form-control" minlength="11" maxlength="11" required>
                            </div>
                            <div id="error-ruc-emisor"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group"><label class="col-md-4 control-label">
                            Importe Total:</label>
                        <div class="col-md-6 control-label">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-info-circle"
                                        data-plugin-tooltip="" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Ingrese el importe total del documento"></i>
                                </span><input id="importeTotal" name="importeTotal" class="form-control" type="number" step="any" required></div>
                            <div id="error-importe-total"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group mb-none"><label class="col-md-4 control-label">Fecha de Emisión:</label>
                        <div class="col-md-6 control-label">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-info-circle"
                                    data-plugin-tooltip="" data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="Ingrese la fecha en que se emitió el documento. Formato: día/mes/año"></i>
                                </span>
                                <input type="date" id="fechaEmision" name="fechaEmision" step="1" class="form-control" min="1990-01-01" max="<?php echo date("Y-m-d");?>" required>
                            </div>
                        </div>
                      
                    
                    </div>
                    {{-- <div class="g-recaptcha" data-sitekey="6LcGMKQZAAAAANO4yW93AeixocfKibwYmU3XsL6K"></div> --}}
                </div>
                <footer class="panel-footer text-center mt-none">
                    <div class="row">
                        <div class="col d-flex justify-content-center">
                            <!-- en data-sitekey cambiar la clave por la clave de stio de web --->
                            <div class="g-recaptcha" data-sitekey="6LciDaUZAAAAACygUSIp3DupIHyhAVKJ_JLJkC6K"></div>
                        </div>
                    </div>
                    <button type="reset" id="btn-limpiar"
                    class="btn btn-3d btn-default" style="min-width:150px"><i class="fas fa-eraser"></i> 
                    Limpiar
                </button>
                <button type="submit" id="btn-submit" class="btn btn-3d btn-tertiary" style="min-width:150px"
                    data-loading-text="buscando...">
                    Consultar
                </button>
                <br/>
                    </footer>
                <div class="header-logo" style="padding-top: 10px; text-align: right"><img id="logo-page" alt="logo"
                        src="https://sos-food.com/wp-content/uploads/2020/03/LOGO-EDITADO-SOSFOOD-1.png" width="99" height="28"></div>
            </section>
        </form>
    </section>

      </div>
    </section>

    @if ($error)
        "<script>
            alert('Sin Resultados');
        </script>"
    @endif
    <script src="https://www.google.com/recaptcha/api.js"></script>

    <script>
         //validacion de los campos  del formulario 

        function checkRuc(target) {
            var  characters = target.value;
            var characters = characters.slice(0, 2);
            console.log(characters);
            if (characters.length == 1) {
                if (!(characters == '1' || characters == '2')) {
                    target.value = '';
                    alert('El campo debe tener un valor inicial de 10 0 20');
                }
            } else if (characters.length == 2) {
                if (!(characters == '10' || characters == '20')) {
                    target.value = '';
                }
            }
        }

        function checkSerie(target) {
            console.log(target.value)
            if (target.value[0] == 'B' || target.value[0] == 'F' || target.value[0] == 'b' || target.value[0] == 'f') {
                target.value = target.value.toUpperCase();
            } else {
            
                alert('El campo debe tener un valor inicial de B o F');
            }
            //validacion de formato
            $(":input").inputmask();
            $("#serieNumero").inputmask({"mask": "a999-999999999"});
        }

        function onSubmit(event) {
            event.preventDefault();
            var  serieNumero = document.querySelector('#serieNumero').value;
            serieNumero
            var ok = grecaptcha.getResponse();
            if (ok) {
                document.querySelector("#demo-form").submit(); 
            } else {
                alert('Es necesario marcar  el captcha');
            }
        }


    </script>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
</body>

</html>
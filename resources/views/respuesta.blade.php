<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel= "stylesheet" href= "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity= "sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin= "anonymous" > 
  <title>Document</title>
</head>
<body style="overflow: hidden">  
  <header class="container-fluid pt-5 pb-3">
    <div class="row">
      <div class="col">
        <img src="{{url('/img/fact.jpeg')}}" width="300rem" alt="">
      </div>
      <div class="col-lg-6">
        <img src="{{url('/img/doc.jpeg')}}" style="width: 30rem; float:right" alt="">
      </div>
    </div>
    <div class="row my-5">
      <div class="col d-flex justify-content-center">
        <a href="#" class="btn btn-sm btn-success mr-2 btn btn-3d" style="min-width:150px" onclick="downloadPdf()">Descargar PDF</a>
        <a href="" class="btn btn-sm btn-primary mr-2 btn-3d" style="min-width:150px">Descargar XML</a>
        <a href="{{route('formulario')}}" class="btn  btn-3d btn-sm btn-danger" style="min-width:150px">Nueva Consulta</a>
      </div>
    </div>
  </header>
  
  <div class="conteiner">
    <div class="row">
      <div class="col"></div>
      <div class="col-md-6" style="height: calc(100vh - 293px);">
        <iframe class="w-100 h-100" id="viewPdf" name="viewPdf" src="" frameborder="0"></iframe>
      </div>
      <div class="col"></div>
    </div>
    {{-- <pre>{!! json_encode($cabeceras[0]) !!}</pre> --}}
    <script type="text/javascript">
      var cabecera = {!! json_encode($cabeceras[0]) !!};                 
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>

 